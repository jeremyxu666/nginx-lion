FROM nginx

WORKDIR /

# Prepare nginx.conf
RUN rm /etc/nginx/conf.d/*
COPY nginx.conf /etc/nginx/conf.d/expertknowledge.conf
COPY proxy_settings.conf /etc/nginx/conf.d/includes/proxy_settings.conf

RUN echo "demouser:Y7uV0JAi4wsYY" > /etc/nginx/users.conf

COPY static /static

CMD nginx -g "daemon off;"
